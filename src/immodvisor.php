<?php
/**
 * Librairie immodvisor
 * Afin de profiter pleinement des mises à jours de cette librarie, il est fortement recommandé de ne pas la modifier
 * @author Jeremy Humbert <jeremy@immodvisor.com>
 * @copyright 2017 immodvisor
 */
namespace Immodvisor;

class Immodvisor {
	
	const VERSION = '1.1.0';
	const URL_API = 'https://api.immodvisor.com/';
	
}
